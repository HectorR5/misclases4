/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author Hector Ramirez
 */
public class Factura {
    
    private int numFactura;
    private String rfc;
    private String nombre;
    private String domicilio;
    private String descripcion;
    private String fecha;
    private float totalVenta;
    
    public Factura(){
        this.numFactura=0;
        this.rfc="";
        this.nombre="";
        this.domicilio="";
        this.descripcion="";
        this.fecha="";
        this.totalVenta=0.0f;
    }

    public Factura(int numFactura, String rfc, String nombre, String domicilio, String descripcion, String fecha, float totalVenta) {
        this.numFactura = numFactura;
        this.rfc = rfc;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.totalVenta = totalVenta;
    }
    
    public Factura(Factura x){
        this.numFactura = x.numFactura;
        this.rfc = x.rfc;
        this.nombre = x.nombre;
        this.domicilio = x.domicilio;
        this.descripcion = x.descripcion;
        this.fecha = x.fecha;
        this.totalVenta = x.totalVenta;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }
    
    
    public float calcularImpuesto(){
        float impuesto=0.0f;
        impuesto=this.totalVenta * .16f;
        return impuesto;
    }
    
    public float calcularPagoTotal(){
        float pagoTotal=0.0f;
        pagoTotal=this.totalVenta+this.calcularImpuesto();
        return pagoTotal;
    }
    
    void imprimirFactura(){
        System.out.println("# Factura = "+this.numFactura);
        System.out.println("RFC = "+this.rfc);
        System.out.println("Nombre = "+this.nombre);
        System.out.println("Domicilio = "+this.domicilio);
        System.out.println("Descripcion = "+this.descripcion);
        System.out.println("Fecha = "+this.fecha);
        System.out.println("Total de Venta = "+this.totalVenta);
        System.out.println("Impuesto(16%) = "+this.calcularImpuesto());
        System.out.println("Total a Pagar = "+this.calcularPagoTotal());
    }
}
