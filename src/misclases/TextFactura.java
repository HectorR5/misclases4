/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;
import java.util.Scanner;
/**
 *
 * @author Hector Ramirez
 */
public class TextFactura {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Factura factura = new Factura();
        Scanner sc = new Scanner(System.in);
        int opcion=0;
        do{
            System.out.println("1. Iniciar Objeto");
            System.out.println("2. Modificar Valores");
            System.out.println("3. Mostrar Informacion");
            System.out.println("4. Salir");
            System.out.print("Dame una opcion: ");
            opcion = sc.nextInt();
            switch(opcion){
                case 1:
                    System.out.println("# de Factura");
                    factura.setNumFactura(sc.nextInt());
                    System.out.println("RFC");
                    factura.setRfc(sc.next());
                    sc.nextLine();
                    System.out.println("Nombre");
                    factura.setNombre(sc.nextLine());
                    System.out.println("Domicilio");
                    factura.setDomicilio(sc.nextLine());
                    System.out.println("Descripcion");
                    factura.setDescripcion(sc.nextLine());
                    System.out.println("Fecha");
                    factura.setFecha(sc.nextLine());
                    System.out.println("Total de Venta");
                    factura.setTotalVenta(sc.nextFloat());
                    break;
                case 2:
                    do{
                        System.out.println("1. # Factura");
                        System.out.println("2. RFC");
                        System.out.println("3. Nombre");
                        System.out.println("4. Domicilio");
                        System.out.println("5. Descripcion");
                        System.out.println("6. Fecha");
                        System.out.println("7. Total de Venta");
                        System.out.println("8. Salir");
                        System.out.print("Dame una opcion: ");
                        opcion = sc.nextInt();
                        switch(opcion){
                            case 1:
                                System.out.println("# de Factura");
                                factura.setNumFactura(sc.nextInt());
                                break;
                            case 2:
                                System.out.println("RFC");
                                factura.setRfc(sc.next());
                                sc.nextLine();
                                break;
                            case 3:
                                System.out.println("Nombre");
                                factura.setNombre(sc.nextLine());
                                break;
                            case 4:
                                System.out.println("Domicilio");
                                factura.setDomicilio(sc.nextLine());
                                break;
                            case 5:
                                System.out.println("Descripcion");
                                factura.setDescripcion(sc.nextLine());
                                break;
                            case 6:
                                System.out.println("Fecha");
                                factura.setFecha(sc.nextLine());
                                break;
                            case 7:
                                System.out.println("Total de Venta");
                                factura.setTotalVenta(sc.nextFloat());
                                break;
                            case 8:
                                System.out.println("Regresar al Menu Inicial");
                                break;
                            default:
                                System.out.println("Opcion no valida");
                                break;
                        }
                    }while(opcion!=8);
                    break;
                case 3:
                    factura.imprimirFactura();
                    System.out.println("Oprima una tecla para continuar");
                    sc.next();
                    break;
                case 4:
                    System.out.println("Gracias...");
                    break;
                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        }while(opcion!=4);
    }
}
