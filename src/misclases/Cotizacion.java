/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author Hector Ramirez
 */
public class Cotizacion {
    private int numCotizacion;
    private String descripcionAuto;
    private float precio;
    private float porcentajePagoInicial;
    private int plazo;
    
    public Cotizacion(){
        this.numCotizacion=0;
        this.descripcionAuto="";
        this.precio=0.0f;
        this.porcentajePagoInicial=0.0f;
        this.plazo=0;
    }
    
    public Cotizacion(int numCotizacion,String descripcionAuto,float precio,float porcentajePagoInicial,int plazo){
        this.numCotizacion=numCotizacion;
        this.descripcionAuto=descripcionAuto;
        this.precio=precio;
        this.porcentajePagoInicial=porcentajePagoInicial;
        this.plazo=plazo;
    }
    
    public Cotizacion(Cotizacion x){
        this.numCotizacion=x.numCotizacion;
        this.descripcionAuto=x.descripcionAuto;
        this.precio=x.precio;
        this.porcentajePagoInicial=x.porcentajePagoInicial;
        this.plazo=x.plazo;
    }

    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcionAuto() {
        return descripcionAuto;
    }

    public void setDescripcionAuto(String descripcionAuto) {
        this.descripcionAuto = descripcionAuto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public void setPorcentajePagoInicial(float porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    public float calcularPagoInicial(){
        float pagoInicial=0.0f;
        pagoInicial=this.precio*(this.porcentajePagoInicial/100);
        return pagoInicial;
    }
    
    public float calcularTotalFinanciar(){
        float totalF=0.0f;
        totalF=this.precio-this.calcularPagoInicial();
        return totalF;
    }
    
    float calcularPagoMensual(){
        float pagoM=0.0f;
        pagoM=this.calcularTotalFinanciar()/this.plazo;
        return pagoM;
    }
    
    void imprimirCotizacion(){
        System.out.println("# Cotizacion = "+this.numCotizacion);
        System.out.println("Descripción = "+this.descripcionAuto);
        System.out.println("Precio = "+this.precio);
        System.out.println("% Pago Inicial = "+this.porcentajePagoInicial);
        System.out.println("Plazo = "+this.plazo);
        System.out.println("Pago Inicial = "+this.calcularPagoInicial());
        System.out.println("Total a Financiar = "+this.calcularTotalFinanciar());
        System.out.println("Pago Mensual = "+this.calcularPagoMensual());
    }
}
