/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

import java.util.Scanner;

/**
 *
 * @author Hector Ramirez
 */
public class TextTerreno {
public static void main(String[] args){
    Terreno terreno = new Terreno();
    Scanner sc = new Scanner(System.in);
    int opcion = 0;
    float perimetro = 0.0f , area = 0.0f , ancho = 0.0f , largo = 0.0f;
    
    
        do {
            System.out.println("1. Iniciar objeto");
            System.out.println("2. Cambiar Ancho");
            System.out.println("3. Cambiar Largo");
            System.out.println("4. Mostrar información");
            System.out.println("5. Salir");
            System.out.print("Dame una opcion: ");
            opcion = sc.nextInt();
            switch(opcion){
                case 1:
                    System.out.println("Dame el ancho del terreno");
                    ancho = sc.nextFloat();
                    System.out.println("Dame el largo del terreno");
                    largo = sc.nextFloat();
                    terreno.setAncho(ancho);
                    terreno.setLargo(largo);
                    break;
                case 2:
                    System.out.println("Dame el ancho del terreno");
                    ancho = sc.nextFloat();
                    terreno.setAncho(ancho);
                    break;
                case 3:
                    System.out.println("Dame el largo del terreno");
                    largo = sc.nextFloat();
                    terreno.setLargo(largo);
                case 4:
                    terreno.imprimirTerreno();
                    break;
                case 5:
                    System.out.println("Hasta la vista baby");
                    break;
                default:
                    System.out.println("No es una opcion valida usa 1-5");
            }
        } while(opcion!=5);
    }
}
