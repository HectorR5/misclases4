/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

import javax.swing.JOptionPane;

/**
 *
 * @author Hector Ramirez
 */
public class JintNotaVenta extends javax.swing.JInternalFrame {

    /**
     * Creates new form JintNotaVenta
     */
    public JintNotaVenta() {
        initComponents();
        this.resize(1100,450);
        this.deshabilitar();
    }
    
    public void deshabilitar(){
        this.txtNumero.setEnabled(false);
        this.txtCliente.setEnabled(false);
        this.txtConcepto.setEnabled(false);
        this.txtTotal.setEnabled(false);
        this.txtDia.setEnabled(false);
        this.txtMes.setEnabled(false);
        this.txtAño.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtTotalPagar.setEnabled(false);
        this.rdbContado.setEnabled(false);
        this.rdbCredito.setEnabled(false);
        
        this.btnMostrar.setEnabled(false);
        this.btnGuardar.setEnabled(false);
    }
    
    public void habilitar(){
        this.txtNumero.setEnabled(!false);
        this.txtCliente.setEnabled(!false);
        this.txtConcepto.setEnabled(!false);
        this.txtTotal.setEnabled(!false);
        this.txtDia.setEnabled(!false);
        this.txtMes.setEnabled(!false);
        this.txtAño.setEnabled(!false);
        this.rdbContado.setEnabled(!false);
        this.rdbCredito.setEnabled(!false);
        this.btnGuardar.setEnabled(!false);
    }
    
    public void limpiar(){
        this.txtNumero.setText("");
        this.txtCliente.setText("");
        this.txtConcepto.setText("");
        this.txtTotal.setText("");
        this.txtDia.setText("");
        this.txtMes.setText("");
        this.txtAño.setText("");
        this.txtImpuesto.setText("");
        this.txtTotalPagar.setText("");
        this.lblFecha.setText("Fecha: ");
        this.rdbContado.setSelected(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtDia = new javax.swing.JTextField();
        txtMes = new javax.swing.JTextField();
        txtAño = new javax.swing.JTextField();
        txtNumero = new javax.swing.JTextField();
        txtCliente = new javax.swing.JTextField();
        txtConcepto = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        rdbCredito = new javax.swing.JRadioButton();
        rdbContado = new javax.swing.JRadioButton();
        btnCerrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        txtImpuesto = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtTotalPagar = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Nota de Venta");
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Palatino Linotype", 1, 18)); // NOI18N
        jLabel1.setText("Tipo:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(60, 250, 150, 30);

        lblFecha.setFont(new java.awt.Font("Palatino Linotype", 1, 18)); // NOI18N
        lblFecha.setText("Fecha:");
        getContentPane().add(lblFecha);
        lblFecha.setBounds(560, 50, 310, 30);

        jLabel3.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        jLabel3.setText("Dia:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(620, 80, 50, 30);

        jLabel4.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        jLabel4.setText("Mes:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(710, 80, 50, 30);

        jLabel5.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        jLabel5.setText("Año:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(800, 80, 50, 30);

        jLabel6.setFont(new java.awt.Font("Palatino Linotype", 1, 18)); // NOI18N
        jLabel6.setText("Numero de Nota:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(60, 50, 150, 30);

        jLabel7.setFont(new java.awt.Font("Palatino Linotype", 1, 18)); // NOI18N
        jLabel7.setText("Cliente:");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(60, 100, 150, 30);

        jLabel8.setFont(new java.awt.Font("Palatino Linotype", 1, 18)); // NOI18N
        jLabel8.setText("Concepto:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(60, 150, 150, 30);

        jLabel9.setFont(new java.awt.Font("Palatino Linotype", 1, 18)); // NOI18N
        jLabel9.setText("Total:");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(60, 200, 150, 30);

        txtDia.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        txtDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiaActionPerformed(evt);
            }
        });
        getContentPane().add(txtDia);
        txtDia.setBounds(600, 110, 69, 30);

        txtMes.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        txtMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMesActionPerformed(evt);
            }
        });
        getContentPane().add(txtMes);
        txtMes.setBounds(690, 110, 69, 30);

        txtAño.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        txtAño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAñoActionPerformed(evt);
            }
        });
        getContentPane().add(txtAño);
        txtAño.setBounds(780, 110, 69, 30);

        txtNumero.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        txtNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumeroActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumero);
        txtNumero.setBounds(230, 50, 140, 30);

        txtCliente.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        txtCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClienteActionPerformed(evt);
            }
        });
        getContentPane().add(txtCliente);
        txtCliente.setBounds(230, 100, 270, 30);

        txtConcepto.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        txtConcepto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtConceptoActionPerformed(evt);
            }
        });
        getContentPane().add(txtConcepto);
        txtConcepto.setBounds(230, 140, 270, 30);

        txtTotal.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        getContentPane().add(txtTotal);
        txtTotal.setBounds(230, 200, 110, 30);

        buttonGroup1.add(rdbCredito);
        rdbCredito.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        rdbCredito.setText("Credito");
        rdbCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbCreditoActionPerformed(evt);
            }
        });
        getContentPane().add(rdbCredito);
        rdbCredito.setBounds(320, 260, 90, 29);

        buttonGroup1.add(rdbContado);
        rdbContado.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        rdbContado.setSelected(true);
        rdbContado.setText("Contado");
        rdbContado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbContadoActionPerformed(evt);
            }
        });
        getContentPane().add(rdbContado);
        rdbContado.setBounds(220, 260, 90, 29);

        btnCerrar.setFont(new java.awt.Font("Palatino Linotype", 0, 14)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(410, 320, 89, 60);

        btnNuevo.setFont(new java.awt.Font("Palatino Linotype", 0, 14)); // NOI18N
        btnNuevo.setText("Nuievo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(920, 40, 90, 60);

        btnGuardar.setFont(new java.awt.Font("Palatino Linotype", 0, 14)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(920, 120, 89, 60);

        btnMostrar.setFont(new java.awt.Font("Palatino Linotype", 0, 14)); // NOI18N
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(920, 200, 89, 60);

        btnLimpiar.setFont(new java.awt.Font("Palatino Linotype", 0, 14)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(150, 320, 89, 60);

        btnCancelar.setFont(new java.awt.Font("Palatino Linotype", 0, 14)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(280, 320, 89, 60);

        jLabel10.setFont(new java.awt.Font("Palatino Linotype", 1, 18)); // NOI18N
        jLabel10.setText("Impuesto:");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(390, 200, 90, 30);

        txtImpuesto.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        txtImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestoActionPerformed(evt);
            }
        });
        getContentPane().add(txtImpuesto);
        txtImpuesto.setBounds(500, 200, 110, 30);

        jLabel11.setFont(new java.awt.Font("Palatino Linotype", 1, 18)); // NOI18N
        jLabel11.setText("Total a Pagar:");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(630, 200, 120, 30);

        txtTotalPagar.setFont(new java.awt.Font("Palatino Linotype", 1, 14)); // NOI18N
        txtTotalPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalPagarActionPerformed(evt);
            }
        });
        getContentPane().add(txtTotalPagar);
        txtTotalPagar.setBounds(750, 200, 110, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDiaActionPerformed

    private void txtMesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMesActionPerformed

    private void txtAñoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAñoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAñoActionPerformed

    private void txtNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumeroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumeroActionPerformed

    private void txtClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClienteActionPerformed

    private void txtConceptoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtConceptoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtConceptoActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void rdbCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbCreditoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdbCreditoActionPerformed

    private void rdbContadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbContadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdbContadoActionPerformed

    private void txtImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestoActionPerformed

    private void txtTotalPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalPagarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalPagarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        if(validar()==false){
            hoy.setDia(Integer.parseInt(this.txtDia.getText()));
            hoy.setMes(Integer.parseInt(this.txtMes.getText()));
            hoy.setAño(Integer.parseInt(this.txtAño.getText()));
            
            nota.setFechaVenta(hoy);
            nota.setNumero(Integer.parseInt(this.txtNumero.getText()));
            nota.setNombrecliente(this.txtCliente.getText());
            nota.setConcepto(this.txtConcepto.getText());
            nota.setTotal(Float.parseFloat(this.txtTotal.getText()));
            
            if(this.rdbContado.isSelected()==true) nota.setTipo(1);
            else nota.setTipo(2);
            
            JOptionPane.showMessageDialog(this, "Se guardo con exito");
            this.btnMostrar.setEnabled(true);
        }
        else{
            JOptionPane.showMessageDialog(this, "Faltó capturar informacion");
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        this.txtNumero.setText(String.valueOf(nota.getNumero()));
        this.txtCliente.setText(nota.getNombrecliente());
        this.txtConcepto.setText(nota.getConcepto());
        this.txtTotal.setText(String.valueOf(nota.getTotal()));
        this.txtImpuesto.setText(String.valueOf(nota.calcularImpuesto()));
        this.txtTotalPagar.setText(String.valueOf(nota.calcularTotalPago()));
        this.txtDia.setText(String.valueOf(hoy.getDia()));
        this.txtMes.setText(String.valueOf(hoy.getMes()));
        this.txtAño.setText(String.valueOf(hoy.getAño()));
        if(nota.getTipo()==1) this.rdbContado.setSelected(true);
        else this.rdbCredito.setSelected(true);
        this.lblFecha.setText("Fecha: "+hoy.fechaFormatoLargo());
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Deseas salir?", "Nota de Venta", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        hoy = new Fecha();
        nota = new NotaVenta();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed
    
    public boolean validar(){
            boolean exito = false;
              if (this.txtNumero.getText().equals(""))exito=true;
              if (this.txtCliente.getText().equals(""))exito=true;
              if (this.txtConcepto.getText().equals(""))exito=true;
              if (this.txtTotal.getText().equals(""))exito=true;
              if (this.txtDia.getText().equals(""))exito=true;
              if (this.txtMes.getText().equals(""))exito=true;
              if (this.txtAño.getText().equals(""))exito=true;
            return exito;
        }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JRadioButton rdbContado;
    private javax.swing.JRadioButton rdbCredito;
    private javax.swing.JTextField txtAño;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtConcepto;
    private javax.swing.JTextField txtDia;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtMes;
    private javax.swing.JTextField txtNumero;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtTotalPagar;
    // End of variables declaration//GEN-END:variables

private Fecha hoy;
private NotaVenta nota;

}
