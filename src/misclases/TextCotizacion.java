/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;
import java.util.Scanner;
/**
 *
 * @author Hector Ramirez
 */
public class TextCotizacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Cotizacion cotizacion = new Cotizacion();
        Scanner sc = new Scanner(System.in);
        int opcion=0;
        do{
            System.out.println("1. Iniciar Objeto");
            System.out.println("2. Modificar Valores");
            System.out.println("3. Mostrar Informacion");
            System.out.println("4. Salir");
            System.out.print("Dame una opcion: ");
            opcion = sc.nextInt();
            switch(opcion){
                case 1:
                    System.out.println("Numero De Cotizacion");
                    cotizacion.setNumCotizacion(sc.nextInt());
                    System.out.println("Descripcion del Auto");
                    cotizacion.setDescripcionAuto(sc.nextLine());
                    System.out.println("Precio");
                    cotizacion.setPrecio(sc.nextFloat());
                    System.out.println("Porcentaje del Pago Inicial");
                    cotizacion.setPorcentajePagoInicial(sc.nextFloat());
                    System.out.println("Plazo");
                    cotizacion.setPlazo(sc.nextInt());
                    break;
                case 2:
                    do{
                        System.out.println("1. # Cotizacion");
                        System.out.println("2. Descripcion");
                        System.out.println("3. Precio");
                        System.out.println("4. % Pago Inicial");
                        System.out.println("5. Plazo");
                        System.out.println("6. Salir");
                        opcion = sc.nextInt();
                        switch(opcion){
                            case 1:
                                System.out.println("Numero De Cotizacion");
                                cotizacion.setNumCotizacion(sc.nextInt());
                                break;
                            case 2:
                                System.out.println("Descripcion del Auto");
                                cotizacion.setDescripcionAuto(sc.nextLine());
                                break;
                            case 3:
                                System.out.println("Precio");
                                cotizacion.setPrecio(sc.nextFloat());
                                break;
                            case 4:
                                System.out.println("Porcentaje del Pago Inicial");
                                cotizacion.setPorcentajePagoInicial(sc.nextFloat());
                                break;
                            case 5:
                                System.out.println("Plazo");
                                cotizacion.setPlazo(sc.nextInt());
                                break;
                            case 6:
                                System.out.println("Regresando al menu inicial");
                                break;
                            default:
                                System.out.println("Opcion no valida");
                                break;
                        }
                    }while(opcion!=6);
                    break;
                case 3:
                    cotizacion.imprimirCotizacion();
                    System.out.println("Oprima una tecla para continuar");
                    sc.next();
                    break;
                case 4:
                    System.out.println("Saliendo");
                    break;
                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        }while(opcion!=4);
    }
    
}
